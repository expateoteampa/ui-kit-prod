const http = require("http");

const hostname = "127.0.0.1";
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/html");
  res.end(
    '<a href="https://docs.google.com/document/d/1oNrxsn-53TwWYdq4oIHmxebkWdmyZxY-8gd-L928zaA/edit?usp=sharing">Voir la prévisualisation des composants et thémes</a>'
  );
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
